# Start with the kong/deck base image
FROM kong/deck:v1.21.0

# Temporarily switch to root
USER root

# Install curl, jq, and coreutils (which includes tee)
RUN apk --no-cache add curl jq coreutils

# Switch back to the original user
USER deckuser
