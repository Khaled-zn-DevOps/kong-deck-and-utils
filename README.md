# Custom Kong/Deck Docker Image

This repository contains a Dockerfile to build a custom Docker image based on the `kong/deck:v1.21.0` image, with additional utilities: curl, jq, and tee.

## Description

The custom Docker image built using this Dockerfile includes:

- **curl**: A powerful tool used to transfer data to and from a server. It supports numerous protocols and is very useful for interacting with APIs and web servers directly from the command line.

- **jq**: A lightweight and flexible command-line JSON processor. It's great for manipulating JSON content on the command line, and in scripts.

- **tee**: A command-line utility that is part of the coreutils package. The `tee` command reads from standard input and writes to standard output and files. It's useful for logging and debugging.

These tools supplement the functionality of `kong/deck` - a command-line tool to configure Kong, the API Gateway, allowing you to manage your Kong setup as code.

## Usage

You can build this Docker image using the `docker build` command:

```bash
docker build -t my-custom-deck-image .
```

Or you can pull a built image of it [from Dockerhub instead](https://hub.docker.com/r/khaleddevopskw/kong-deck-and-utils). This repo will not be maintained, or maybe barely.

## Contributing

Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change. I'll just merge it whenever I notice it, which would be rarely.

## License

[MIT](https://choosealicense.com/licenses/mit/)

Please note that this image is not officially maintained by Kong Inc. Please use discretion when deploying in production environments.